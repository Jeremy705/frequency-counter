function countFrequencies() {
    let letterCounts = {};
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
for (let i = 0; i < typedText.length; i++) {
    currentLetter = typedText[i];
    if (letterCounts[currentLetter] === undefined) {
        letterCounts[currentLetter] = 1;  
    } else {  
        letterCounts[currentLetter]++;  
    }
    
}
for (let letter in letterCounts) {  
    const span = document.createElement("span");  
    // const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");  
    // span.appendChild(textContent);  
    // document.getElementById("lettersDiv").appendChild(span);  
    document.getElementById("lettersDiv").appendChild(span);
    span.innerHTML += '"' + letter + "\": " + letterCounts[letter] + "<br> ";
}
    let words = typedText.split(/\s/); 
    let wordCounts = {};
for ( let i = 0; i < words.length; i++) {
    let currentword = words[i];
    if (wordCounts[currentword] === undefined) {
        wordCounts[currentword] = 1;
    } else{
        wordCounts[currentword]++;
    }
}
 for (let word in wordCounts) {
     const span2 = document.createElement("span");
    //  const textContent2 = document.createTextNode('"' + word + "\": " + wordCounts[word] + " , ");
    //  span2.appendChild(textContent2);
    //  document.getElementById("wordsDiv").appendChild(span2);
    document.getElementById("wordsDiv").appendChild(span2);
    span2.innerHTML += '"' + word + "\": " + wordCounts[word] + "<br> ";
 } 
}

document.getElementById("countButton").addEventListener("click", countFrequencies);
     
